import * as React from 'react';
import { Component } from 'react-simplified';
import ReactDOM from 'react-dom';
import { NavLink, HashRouter, Route } from 'react-router-dom';
import { connection } from './mysql_connection';

class StudentList extends Component {
  students = [];

  render() {
    return (
      <div style={{border: "solid black", backgroundColor: "darkgrey"}}>
        <ul>
          {this.students.map(student => (
            <li key={student.id}>
              <NavLink activeStyle={{ color: 'darkblue' }} to={'/students/' + student.id}>
                {student.name}
              </NavLink>
            </li>
          ))}
        </ul>
      </div>
    );
  }

  mounted() {
    connection.query('select id, name from Students', (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return

      this.students = results;
    });
  }
}

class StudentDetails extends Component {
  name = '';
  email = '';
  subjects = [];

  render() {
    return (
      <div style={{border: "solid black", backgroundColor: "pink", color: "teal"}}>
        <ul>
          <li>Name: {this.name}</li>
          <li>Email: {this.email}</li>
          <li>Subjects:
            <ul>
              {this.subjects.map(subject => (
                <li key={subject.id}>{subject.name}</li>
              ))}
            </ul>
          </li>
        </ul>
      </div>
    );
  }

  mounted() {
    connection.query('select name, email from Students where id=?', [this.props.match.params.id], (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return

      this.name = results[0].name;
      this.email = results[0].email;
    });

    let sql = "select S.id, S.name\n" +
      "from Students\n" +
      "inner join Student_has_subject Shs on Students.id = Shs.stud_id\n" +
      "inner join Subjects S on Shs.subj_id = S.id\n" +
      "where Students.id = ?";
    connection.query(sql, [this.props.match.params.id], (error, result) => {
      if (error) return console.error(error);
      this.subjects = result;
    })
  }
}

class Tabs extends Component {

  render() {
    return (
      <div>
        <ul className="nav nav-tabs">
          <li className="nav-item">
            <NavLink className="nav-link" to={"/whiteboard"}>Whiteboard</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to={"/students"}>Students</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to={"/subjects"}>Subjects</NavLink>
          </li>
        </ul>
      </div>
    )
  }
}

class SubjectList extends Component {
  subjects = [];

  render() {
    return (
      <div style={{border: "solid black", backgroundColor: "darkgrey"}}>
        <ul>
          {this.subjects.map(subject => (
            <li key={subject.id}>
              <NavLink activeStyle={{ color: 'darkblue' }} to={'/subjects/' + subject.id}>
                {subject.name}
              </NavLink>
            </li>
          ))}
        </ul>
      </div>
    );
  }

  mounted() {
    connection.query("select id, code, name from Subjects", (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return

      this.subjects = results;
    });
  }
}

class SubjectDetails extends Component {
  name = "";
  code = "";
  students = [];

  render() {
    return(
      <div style={{border: "solid black", backgroundColor: "pink", color: "teal"}}>
        <ul>
          <li>Code: {this.code}</li>
          <li>Name: {this.name}</li>
          <li>Students who take this subject:
            <ul>
              {this.students.map(student => (
                <li key={student.id}>{student.name}</li>
              ))}
            </ul>
          </li>
        </ul>
      </div>
    );
  }

  mounted() {
    connection.query('select id, code, name from Subjects where id=?', [this.props.match.params.id], (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return

      this.name = results[0].name;
      this.code = results[0].code;
    });

    let sql = "select Students.id, Students.name\n" +
      "from Students\n" +
      "inner join Student_has_subject Shs on Students.id = Shs.stud_id\n" +
      "inner join Subjects S on Shs.subj_id = S.id\n" +
      "where S.id = ?";
    connection.query(sql, [this.props.match.params.id], (error, results) => {
      if (error) return console.error(error);

      this.students = results;
    });
  }
}

ReactDOM.render(
  <HashRouter>
    <div>
      <Tabs/>
      <Route path="/students" component={StudentList}/>
      <Route path="/subjects" component={SubjectList}/>
      <Route path="/students/:id" component={StudentDetails} />
      <Route path="/subjects/:id" component={SubjectDetails}/>
    </div>
  </HashRouter>,
  document.getElementById('root')
);
